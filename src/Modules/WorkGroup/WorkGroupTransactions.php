<?php

namespace Foodsharing\Modules\WorkGroup;

use Foodsharing\Lib\Db\Mem;
use Foodsharing\Modules\Region\ForumFollowerGateway;
use Foodsharing\Utility\EmailHelper;
use Symfony\Contracts\Translation\TranslatorInterface;

class WorkGroupTransactions
{
    public function __construct(
        private readonly Mem $mem,
        private readonly WorkGroupGateway $workGroupGateway,
        private readonly ForumFollowerGateway $forumFollowerGateway,
        private readonly EmailHelper $emailHelper,
        private readonly TranslatorInterface $translator
    ) {
    }

    /**
     * Removes a user from a working group and cancels the forum subscriptions.
     *
     * @throws \Exception
     */
    public function removeMemberFromGroup(int $groupId, int $memberId): void
    {
        $this->forumFollowerGateway->deleteForumSubscription($groupId, $memberId);
        $this->workGroupGateway->removeFromGroup($groupId, $memberId);
    }

    /**
     * Checks if an user is administrator for a working group.
     *
     * INFO: The reference information is cached on redis and the cache is updated by @see MaintenanceControl.
     *
     * @param int $userId UserId to check
     *
     * @return bool True is administrator, False no information present
     */
    public function isAdminForAWorkGroup(int $userId): bool
    {
        if ($allGroupAdmins = $this->mem->get('all_global_group_admins')) {
            return in_array($userId, unserialize($allGroupAdmins));
        }

        return false;
    }

    public function sendMailToGroup(string $groupName, string $message, string $username, int $userId, array $recipients, string $userMail): void
    {
        $this->emailHelper->tplMail('general/workgroup_contact', $recipients, [
            'gruppenname' => $groupName,
            'message' => $message,
            'username' => $username,
            'userprofile' => BASE_URL . '/profile/' . $userId,
        ], $userMail);
    }

    public function requestToGroup(int $groupId, int $userId, string $motivation, string $ability, string $experience, int $selectedTime): void
    {
        $content = [
            $this->translator->trans('group.entermotivation') . "\n===========\n" . trim($motivation),
            $this->translator->trans('group.enterskills') . "\n============\n" . trim($ability),
            $this->translator->trans('group.enterxp') . "\n==========\n" . trim($experience),
            $this->translator->trans('group.entertime') . "\n=====\n" . $selectedTime,
        ];

        $this->workGroupGateway->groupApply($groupId, $userId, implode("\n\n", $content));
        $groupMail = $this->workGroupGateway->getGroupMail($groupId);
        $group = $this->workGroupGateway->getGroup($groupId);

        $userWithMail = $this->workGroupGateway->getFsWithMail($userId);

        $link = BASE_URL . '/?page=application&bid=' . $groupId . '&fid=' . $userId;

        $this->emailHelper->libmail(
            [
                'email' => $userWithMail['email'],
                'email_name' => $userWithMail['name'],
            ],
            $groupMail,
            $this->translator->trans('group.apply.title', ['{group}' => $group['name']]),
            nl2br($this->translator->trans('group.apply.summary', [
                    '{name}' => $userWithMail['name'],
                    '{group}' => $group['name'],
                ]) . "\n\n" . implode("\n\n", $content) . "\n\n"
                . $this->translator->trans('group.apply.link_description')
                . ' <a href="' . $link . '">' . $link . '</a>')
        );
    }
}
