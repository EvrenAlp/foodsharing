# Explanations and usability

Some parts of the website are not intuitive to new users. For those cases please use the `Info.vue` component.
Please make sure not to create duplicates of wiki-articles. This should only be used for small tips or include a link to the wiki article.